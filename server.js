#!/usr/bin/env node
const { existsSync, readFileSync, lstatSync } = require('fs');
const { resolve } = require('path');
const express = require('express');
const argv = require('yargs').argv;

const ensureRight = (str, end) => (str.endsWith(end) ? str : `${str}${end}`);
const ensureLeft = (str, begin) => (str.startsWith(begin) ? str : `${begin}${str}`);

const port = argv.port || 3001;
const basePath = argv.basePath ? ensureRight(ensureLeft(argv.basePath, '/'), '/') : '/';
const app = express();

const handleOptions = (req, res, next) => 'OPTIONS' === req.method ? res.sendStatus(200) : next();

const allowCORS = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.header('origin'));
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
};

const setContentType = (req, res, next) => {
  res.set('Content-Type', 'text/xml');
  next();
}

const indexContent = readFileSync(resolve(__dirname, 'www', 'index.html'));
const handleResponse = (res, resourcePath) => {
  const filePath = resolve(__dirname, 'www', resourcePath);
  if (existsSync(filePath) && !lstatSync(filePath).isDirectory()) {
    if (filePath.endsWith('.js')) {
      res.set('Content-Type', 'application/javascript');
    } else if (filePath.endsWith('.css')) {
      res.set('Content-Type', 'text/css');
    } else if (filePath.endsWith('.html')) {
      res.set('Content-Type', 'text/html');
    } else if (filePath.endsWith('.png')) {
      res.set('Content-Type', 'image/png');
    } else if (filePath.endsWith('.svg')) {
      res.set('Content-Type', 'image/svg+xml');
    }
    return res.send(readFileSync(filePath));
  } else {
    res.set('Content-Type', 'text/html');
    res.send(indexContent);
  }
}

app.get(`${basePath}*`, handleOptions, allowCORS, setContentType, (req, res, next) => {
  handleResponse(res, req.path.replace(basePath, ''));
});

if (basePath.length > 1) {
  const basePathWithoutRightSlash = basePath.slice(0, basePath.length - 1);
  app.get(`${basePathWithoutRightSlash}*`, handleOptions, allowCORS, setContentType, (req, res, next) => {
    handleResponse(res, req.path.replace(basePathWithoutRightSlash, ''));
  });
}

app.listen(port);
console.log('Add files to www');
console.log('Now listening on port [%s]', port);

process.on('uncaughtException', err => {
  console.log(err);
});
